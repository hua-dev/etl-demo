import argparse
import json
import logging
import time

from kafka import KafkaProducer
from neo4j_base import SimpleNeo4jConnector

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--log-level', action='store', default='INFO', type=str, required=False)
arg_parser.add_argument('--read-step-records', action='store', default=5, type=int, required=False)
arg_parser.add_argument('--read-step-delay', action='store', default=20, type=int, required=False)

logging.basicConfig()
LOGGER = logging.getLogger('neo4j_producer')

TOPIC_NAME = 'users-topic'


class GraphProducer:

    def __init__(self):
        self.neo = SimpleNeo4jConnector()
        self.producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                                      value_serializer=lambda m: json.dumps(m).encode('utf-8'))

    def produce(self, step, delay):
        skip = 0
        while True:
            users = self.neo.query_users(skip, step)
            if users:
                skip += step
                for u in users:
                    LOGGER.info('Sending message [name={}, products={}]'.format(u.name, u.products))
                    self.producer.send(TOPIC_NAME, json.dumps(u.__dict__))
                time.sleep(delay)
            else:
                break
        self.neo.close()
        self.producer.flush()


def set_log_level(log_level):
    all_levels = ['critical', 'error', 'warning', 'info', 'debug']
    if log_level.lower() not in all_levels:
        LOGGER.error('Unrecognized log level, must be one of {}'.format(all_levels))
        exit(0)
    LOGGER.setLevel(log_level.upper())


if __name__ == '__main__':
    args = arg_parser.parse_args()
    set_log_level(args.log_level)

    graph_producer = GraphProducer()
    graph_producer.produce(args.read_step_records, args.read_step_delay)
