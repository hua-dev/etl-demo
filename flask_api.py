import argparse
import logging

from flask import Flask, jsonify, abort
from consumer import find_user_products

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--log-level', action='store', default='INFO', type=str, required=False)

logging.basicConfig()
LOGGER = logging.getLogger('flask_app')

flask_app = Flask(__name__)


@flask_app.route('/')
def say_hello():
    LOGGER.info('Caught request to "/"')
    return 'Hello there!'


@flask_app.route('/user/<name>')
def get_user(name):
    LOGGER.info('Caught request to "/user/{}"'.format(name))
    response_raw = find_user_products(name)
    if not response_raw and response_raw != []:
        abort(404)
    return jsonify(response_raw)


@flask_app.errorhandler(404)
def user_not_found(error):
    return 'User not found', 404
