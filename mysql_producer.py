import argparse
import json
import logging
import time

import mysql.connector
from kafka import KafkaProducer
from mysql.connector import errorcode

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--log-level', action='store', default='INFO', type=str, required=False)
arg_parser.add_argument('--read-step-records', action='store', default=10, type=int, required=False)
arg_parser.add_argument('--read-step-delay', action='store', default=20, type=int, required=False)

logging.basicConfig()
LOGGER = logging.getLogger('mysql_producer')

TOPIC_NAME = 'products-topic'


class ERProducer:

    def __init__(self):
        self.producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                                      value_serializer=lambda m: json.dumps(m).encode('utf-8'))
        try:
            self.cnx = mysql.connector.connect(user='dbuser', password='dbuser', host='localhost', database='productdb')
            self.cursor = self.cnx.cursor()
        except mysql.connector.Error as error:
            if error.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                LOGGER.error('[mysql] Bad username/password')
            elif error.errno == errorcode.ER_BAD_DB_ERROR:
                LOGGER.error('[mysql] Unknown database name')
            else:
                LOGGER.error(error)

    def produce(self, step, delay):
        offset = 0
        while True:
            if self.cursor:
                self.cursor.execute('SELECT id, name FROM product ORDER BY id LIMIT {} OFFSET {}'
                                    .format(step, offset))
                for (p_id, p_name) in self.cursor:
                    LOGGER.info('Sending message [id={}, name={}]'.format(p_id, p_name))
                    self.producer.send(TOPIC_NAME, {'id': p_id, 'name': p_name})
                offset += step
                time.sleep(delay)
            else:
                break
        self.cursor.close()
        self.cnx.close()
        self.producer.flush()


def set_log_level(log_level):
    all_levels = ['critical', 'error', 'warning', 'info', 'debug']
    if log_level.lower() not in all_levels:
        LOGGER.error('Unrecognized log level, must be one of {}'.format(all_levels))
        exit(0)
    LOGGER.setLevel(log_level.upper())


if __name__ == '__main__':
    args = arg_parser.parse_args()
    set_log_level(args.log_level)

    producer = ERProducer()
    producer.produce(args.read_step_records, args.read_step_delay)
