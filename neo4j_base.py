import argparse
import random
import timeit
import logging

from neo4j import GraphDatabase

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--log-level', action='store', default='INFO', type=str, required=False)
arg_parser.add_argument('--max-users', action='store', default=1000, type=int, required=False)
arg_parser.add_argument('--max-products', action='store', default=1000, type=int, required=False)
arg_parser.add_argument('--max-user-friendships', action='store', default=10, type=int, required=False)
arg_parser.add_argument('--max-user-products', action='store', default=10, type=int, required=False)

logging.basicConfig()
LOGGER = logging.getLogger('neo4j_base')


class SimpleNeo4jConnector:

    def __init__(self):
        self.driver = GraphDatabase.driver('bolt://localhost:7687', auth=('neo4j', 'mypassword'))

    def close(self):
        self.driver.close()

    def query_users(self, skip, limit):
        with self.driver.session() as session:
            result = session.write_transaction(self._query_and_return_users, skip, limit)
            return list(map(lambda u: User(u._properties['name'], u._properties['products']), result))

    def create_user(self, name, products):
        with self.driver.session() as session:
            user_id = session.write_transaction(self._create_and_return_user, name, products)
            if user_id >= 0:
                LOGGER.debug('User created, id=' + str(user_id))

    def create_friendship(self, name_1, name_2):
        with self.driver.session() as session:
            result = session.write_transaction(self._create_and_return_friendship, name_1, name_2)
            if result == 'FRIEND_OF':
                LOGGER.debug(name_1 + '-[FRIEND_OF]->' + name_2)

    @staticmethod
    def _create_and_return_user(tx, name, products):
        result = tx.run('CREATE (a:User) '
                        'SET a.name = $name '
                        'SET a.products = $products '
                        'RETURN id(a)', name=name, products=products)
        return result.single()[0]

    @staticmethod
    def _create_and_return_friendship(tx, name_1, name_2):
        query = (
            'MATCH '
            '  (a:User),'
            '  (b:User) '
            'WHERE a.name=$name1 AND b.name=$name2 '
            'CREATE (b) -[r:FRIEND_OF]->(a) '
            'RETURN type(r)'
        )
        result = tx.run(query, name1=name_1, name2=name_2)
        return result.single()[0]

    @staticmethod
    def _query_and_return_users(tx, skip, limit):
        result = tx.run('MATCH (n:User) RETURN n ORDER BY n.name SKIP $skip LIMIT $limit',
                        skip=skip, limit=limit)
        values = []
        for record in result:
            values.append(record.values()[0])
        return values


def set_log_level(log_level):
    all_levels = ['critical', 'error', 'warning', 'info', 'debug']
    if log_level.lower() not in all_levels:
        LOGGER.error('Unrecognized log level, must be one of {}'.format(all_levels))
        exit(0)
    LOGGER.setLevel(log_level.upper())


def init_neo4j():
    args = arg_parser.parse_args()
    set_log_level(args.log_level)

    USER_COUNT = args.max_users
    PRODUCT_COUNT = args.max_products
    MAX_USER_FRIENDS_COUNT = args.max_user_friendships
    MAX_USER_PRODUCTS_COUNT = args.max_user_products

    LOGGER.info('[init_new4j] Operation started')
    start = timeit.default_timer()
    neo = SimpleNeo4jConnector()
    for i in range(USER_COUNT):
        idx = i + 1
        user_products = []
        all_products = [*range(1, PRODUCT_COUNT + 1)]
        for _ in range(random.randint(0, MAX_USER_PRODUCTS_COUNT)):
            p_choice = random.choice(all_products)
            user_products.append(p_choice)
            all_products.remove(p_choice)
        neo.create_user('user' + str(idx), user_products)
        LOGGER.debug('Created user {}'.format(str(idx)))
    friendship_count = 0
    for i in range(USER_COUNT):
        idx = i + 1
        available_users = [*range(1, USER_COUNT + 1)]
        available_users.remove(idx)
        for _ in range(random.randint(1, MAX_USER_FRIENDS_COUNT)):
            f_choice = random.choice(available_users)
            neo.create_friendship('user' + str(idx), 'user' + str(f_choice))
            LOGGER.debug('Created friendship between {} and {}'.format(str(idx), str(f_choice)))
            friendship_count += 1
            available_users.remove(f_choice)
    neo.close()
    end = timeit.default_timer()
    LOGGER.info('[init_neo4j] Users created: ' + str(USER_COUNT) +
                ', relationships created: ' + str(friendship_count) +
                ', elapsed time: ' + str(end - start) + 's')


class User:
    def __init__(self, name, products):
        self.name = name
        self.products = products


if __name__ == '__main__':
    init_neo4j()
