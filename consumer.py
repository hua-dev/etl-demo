import argparse
import json
import logging

import pymongo
from kafka import KafkaConsumer
from pymongo import MongoClient

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--log-level', action='store', default='INFO', type=str, required=False)

logging.basicConfig()
LOGGER = logging.getLogger('consumer')

mongo = MongoClient('localhost', 27017, username='root', password='example')
db = mongo['fusion']
col_users = db['users']
col_products = db['products']


def find_user_products(name):
    try:
        return col_users.find_one({'name': name})['products']
    except TypeError:
        LOGGER.info('User {} has made no purchases'.format(name))
    except:
        LOGGER.error('Something went wrong')
        return ''


def get_or_dummy(item_id, solid_list):
    for item in solid_list:
        if isinstance(item, str):
            return {'id': item_id}
        elif item['id'] == item_id:
            return item
    return {'id': item_id}


def update_user_product(user, product):
    user_products = list(user['products'])
    if {'id': product['id']} in user_products:
        new_user_products = []
        for p in user_products:
            if p['id'] == product['id']:
                new_user_products.append(product)
            else:
                new_user_products.append(p)
        user['products'] = new_user_products
    return user


def persist_user(user):
    solid_user_products = list(col_products.find({'id': {'$in': user['products']}}, {"_id": 0}))
    new_products = []
    for p in user['products']:
        new_products.append(get_or_dummy(p, solid_user_products))

    user['products'] = new_products
    new_user = col_users.insert_one(user)
    LOGGER.info('New user added, name={}'.format(user['name']))


def persist_product(product):
    # fetch users that have a dummy product version (id only)
    # they are users already stored with simple product version because these products were not published yet
    users_with_product = list(col_users.find({'products': {'id': product['id']}}))
    for user in users_with_product:
        user = update_user_product(user, product)
        col_users.update_one(
            {'name': user['name']},
            {'$set': {'products': user['products']}}
        )
    # and also save the product for future encounters
    col_products.insert_one(product)
    LOGGER.info('New product added, id={}'.format(product['id']))


def set_log_level(log_level):
    all_levels = ['critical', 'error', 'warning', 'info', 'debug']
    if log_level.lower() not in all_levels:
        LOGGER.error('Unrecognized log level, must be one of {}'.format(all_levels))
        exit(0)
    LOGGER.setLevel(log_level.upper())


if __name__ == '__main__':
    args = arg_parser.parse_args()
    set_log_level(args.log_level)

    try:
        mongo.drop_database('fusion')
    except pymongo.errors.ServerSelectionTimeoutError:
        LOGGER.info('Cannot drop database "fusion", maybe it is not created yet...')
    db = mongo['fusion']

    consumer = KafkaConsumer('python-group',
                             bootstrap_servers=['localhost:9092'],
                             value_deserializer=lambda m: json.loads(m.decode('utf-8')))

    consumer.subscribe(['users-topic', 'products-topic'])
    LOGGER.info('Subscribed to topics, waiting for events...')
    for message in consumer:
        if message.topic == 'users-topic':
            LOGGER.debug('%s:%d:%d value=%s' % (message.topic, message.partition, message.offset, message.value))
            persist_user(json.loads(message.value))
        else:
            LOGGER.debug('%s:%d:%d value=%s' % (message.topic, message.partition, message.offset, message.value))
            persist_product(message.value)


